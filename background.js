function getLocation(href) {
    var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
        href: href,
        protocol: match[1],
        host: match[2],
        hostname: match[3],
        port: match[4],
        pathname: match[5],
        search: match[6],
        hash: match[7]
    }
}

async function redirect(requestDetails) {
    // Parse the current URL
    var url = getLocation(requestDetails.url);

    // Figure out the IP we are requesting
    record = await browser.dns.resolve(url.hostname);

    // Check if the IP we are looking for is in the IP list
    if (record.addresses.includes("192.168.24.2")) {
        // The page was blocked
        return {
            redirectUrl: "http://proxy/blocked?url=" + requestDetails.url
        }
    }
}

browser.webRequest.onBeforeRequest.addListener(
    redirect,
    {urls:["<all_urls>"]},
    ["blocking"]
);